## What is software testing?
When a software is under test, it requires a source of test inputs which after treatment by the software result in test outputs. The outputs are processed by a sets of expactation and/or assertions checks. If the outputs satisfies the expactations and/or assertions, then the test case has succeeded. If the outputs are not okay, then the test case has failed.
-![alt text](http://lh3.ggpht.com/C4oIZavjVkiDrLb0pMO5uIUJFJ4QVlgN_N7o6nUFJyuGqxr43-kWTB35-Sokg47LBT2Ni-AlHy9NY8X9BqYi=s5004 "Logo Title Text 1")

## Why is software testing so important?
 - **Consistancy:** testing ensures that codeis consitant with the functionality and the algorithm  it is trying to implement. Testing allows engineers to have confidence that the code is doing what it is supoposed to do.
 - **Robustness:** testing also ensure that products and development teams have the necessary harness to be able to innovate and take risk. When implementing functional evolutions the code is not breaking and if it does the necessary feedback are given by tests and the issue fixed.
 - **Architecture:** Testing is the best way to verify the relevancy of an architecture design, the harder it is to write tests for the multiple compoments of the architectures, the harder it will be to implement that architecture into code.

## What is tested in a software and how?
 - **Isolated parts:** Testing one small software module at a time and testing it in an isolated fashion to make sure it is doing what it is supposed to independant of what other small module of the software are doing. This is done through *Unit Testing.*
 - **Parts together:** This refers to looking at multiple software modules that have previously been unit tested and testing them in combination with each other. This is done though *Integrating Testing.*
 - **Entire System:** This refers to look at all modules as whole, which is an application, a software or an api and see if it meets its goals as a computing system. 
   - **Functionalities:**  The system is tested in a real-world scenario with integration with database, network, webservices and other external elements. This is done through End2End Testing.
   - **Behaviours:** The system is tested again in a real world scenario including interaction with necessary external elements, the focus is on how the ssytem behave on chosen scenario that targets specific accetance level of the software. Acceptance Testing which is at the heart of the BDD Approache.
