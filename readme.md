#Testing Best Practices

## Preambule
The following guidelines, principles and practices assume that development are done with the Tests Driven Development and Behaviour Driven Development approaches.


## Description 
This [proposal] document contains principles and practices for consitant testing of applications and apis. Those principles are the results of an extensive analisys unit and behavior tests harness of a large PHP API. All tests examples used here have been written with PHPUnit and Behat. It is thus believable that they can be applied with small mofications to any development stack/context with xUnit, Rspec and/or Gherkin based test frameworks.

## Content

- [Basic Principles](1.basic-principles.md)
- [Unit Testing](2.unit-testing.md)
- [Behaviour Testing](3.behaviour-testing.md)
- [Integration Testing](4.integration-testing.md)
- [End2End Testing](5.end2end-testing.md)
- Coding-Lab Example, in `Coding-Lab/`
